const bars = document.querySelector(".btn-toggle");
const navbarMenu = document.querySelector(".navbar-menu");
const close = document.querySelector(".btn-close-navbar");
const toggleItems = document.querySelectorAll(".dropdown-item");
const menuToggles = document.querySelectorAll(".dropdown-item-toggle");
const arrowDowns = document.querySelectorAll(".dropdown-item i");
const htmlTab = document.querySelector("html");

bars.addEventListener("click",()=>{
    navbarMenu.classList.remove("hidden-navbar");
    htmlTab.style = "overflow:hidden;";
    setTimeout(() => {
        navbarMenu.classList.add("show-navbar");
    },100);
});
close.addEventListener("click", ()=>{
    htmlTab.style = "";
    navbarMenu.classList.remove("show-navbar");
    setTimeout(() => {
        navbarMenu.classList.add("hidden-navbar");
    },200);
});

for(let i = 0; i<toggleItems.length; i++){
    toggleItems[i].addEventListener("click",()=>{
        if(menuToggles[i].classList.contains("show-dropdown-item-toggle")){
            menuToggles[i].classList.remove("show-dropdown-item-toggle");
            arrowDowns[i].classList.remove("rotate-icon");
            
        }else{
            menuToggles[i].classList.add("show-dropdown-item-toggle");
            arrowDowns[i].classList.add("rotate-icon");
        }
    })
}