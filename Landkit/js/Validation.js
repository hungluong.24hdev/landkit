const nameInput = document.querySelector(".name-input");
const emailInput = document.querySelector(".email-input");
const passwordInput = document.querySelector(".password-input");
const tf_name = document.querySelector(".name-field");
const tf_email = document.querySelector(".email-field");
const tf_password = document.querySelector(".password-field");
const error_name = document.querySelector(".error-name");
const error_email = document.querySelector(".error-email");
const error_password = document.querySelector(".error-password");
const btn_download = document.querySelector(".btn-download");

const validateName = () => {
    let name = tf_name.value;
    if (!name.length > 0) {
        error_name.innerHTML = "Name is required!";
        return false;
    } else {
        error_name.innerHTML = "";
        return name;
    }
};
const validatePassword = () => {
    let password = tf_password.value;
    if (password.length === 0) {
        error_password.innerHTML = "Password is required!";
        return false;
    } else if (password.length < 8) {
        error_password.innerHTML = "Password must be at least 8 characters";
        return false;
    } else {
        error_password.innerHTML = "";
        return password;
    }
};
const validateEmail = () => {
    const regexEmail = /^[\w-\.]+@([\w]+\.)+[\w]{2,4}$/;
    let email = tf_email.value;
    if (!email.length > 0) {
        error_email.innerHTML = "Email is required!";
        return false;
    } else if (!regexEmail.test(email)) {
        error_email.innerHTML = "Email is invalid!";
        return false;
    } else {
        error_email.innerHTML = "";
        return email;
    }
};

tf_name.addEventListener("blur", (e) => {
    validateName();
});
tf_email.addEventListener("blur", (e) => {
    validateEmail();
});
tf_password.addEventListener("blur", (e) => {
    validatePassword();
});

btn_download.addEventListener("click", (e) => {
    e.preventDefault();
    const name = validateName();
    const email = validateEmail();
    const password = validatePassword();
    if (name && email && password) {
        console.log(`name: ${name}, email: ${email}, password: ${password}`);
    } else {
        console.log("error");
    }
});
