const allTexts = ["developers.", "founders.", "designers."];
let countText = 0;
let index = 0;
let currentText = " ";
let letter = "";

const runningText = document.querySelector(".running-text");
let write = true;

function Type() {
    if (index < 0) {
        write = true;
        // console.log(countText);
        ++countText;
        ++index;
    }
    if (countText === allTexts.length) {
        countText = 0;
        // console.log(currentText);
    }

    currentText = allTexts[countText];

    if (write === true) {
        letter = currentText.slice(0, index++);
    } else {
        letter = currentText.slice(0, index--);
    }
    runningText.innerText = letter;

    if (letter.length === currentText.length) {
        setTimeout(() => {
            write = false;
        }, 1000);
    }
    setTimeout(Type, 100);
}
Type();
