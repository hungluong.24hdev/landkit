const pre = document.querySelector(".btn-pre");
const next = document.querySelector(".btn-next");
const test = document.querySelector(".image-slide-2");
const textSlideBody = document.querySelector(".text-slide-body");
const textSlides = document.querySelectorAll(".text-slide");
const imgSlides = document.querySelectorAll(".image-slide");

//click
next.addEventListener("click", () => {
    for (let i = 0; i < textSlides.length; i++) {
        if (textSlides[i].classList.contains("show")) {
            textSlides[i].classList.remove("show");
            textSlides[i].style = "animation: switch-left-hidden 0.4s ease";
            imgSlides[i].classList.remove("show");
        } else {
            textSlides[i].classList.add("show");
            textSlides[i].style = "animation: switch-left-show 0.4s ease";
            imgSlides[i].classList.add("show");
        }
    }
});
pre.addEventListener("click", () => {
    for (let i = 0; i < textSlides.length; i++) {
        if (textSlides[i].classList.contains("show")) {
            textSlides[i].classList.remove("show");
            textSlides[i].style = "animation: switch-right-hidden 0.4s ease";
            imgSlides[i].classList.remove("show");
        } else {
            textSlides[i].classList.add("show");
            textSlides[i].style = "animation: switch-right-show 0.4s ease";
            imgSlides[i].classList.add("show");
        }
    }
});
//touch
let isDown = false;
let start;
let slideWidth;
let distance1 = 0;
let distance2;
for (let i = 0; i < textSlides.length; i++) {
    textSlides[i].addEventListener("mousedown", (e) => {
        textSlideBody.style.cursor = "grabbing";
        console.log("down");
        isDown = true;
        start = e.clientX;
        slideWidth = textSlides[0].clientWidth;
        distance1 = 0;
    });
}
document.querySelector("html").addEventListener("mouseup", () => {
    if (isDown) {
        textSlideBody.style.cursor = "grab";
        isDown = false;
        if (distance1 > 0) {
            if (distance1 > slideWidth / 2) {
                for (let i = 0; i < textSlides.length; i++) {
                    if (textSlides[i].classList.contains("show")) {
                        textSlides[
                            i
                        ].style = `transform: translateX(${slideWidth}px); transition: 0.5s`;
                        imgSlides[i].classList.remove("show");
                        setTimeout(() => {
                            textSlides[i].classList.remove("show");
                            textSlides[i].style = "";
                        }, 500);
                    } else {
                        textSlides[
                            i
                        ].style = `transform: translateX(0); transition: 0.5s; opacity: 1`;
                        imgSlides[i].classList.add("show");
                        setTimeout(() => {
                            textSlides[i].classList.add("show");
                            textSlides[i].style = "";
                        }, 500);
                    }
                }
            } else {
                for (let i = 0; i < textSlides.length; i++) {
                    if (textSlides[i].classList.contains("show")) {
                        textSlides[
                            i
                        ].style = `transform: translateX(${0}); transition: 0.5s`;
                        setTimeout(() => {
                            textSlides[i].style = "";
                        }, 500);
                    } else {
                        textSlides[
                            i
                        ].style = `transform: translateX(${-slideWidth}px); transition: 0.5s`;
                        setTimeout(() => {
                            textSlides[i].style = "";
                        }, 500);
                    }
                }
            }
        } else {
            if (distance1 < -slideWidth / 2) {
                for (let i = 0; i < textSlides.length; i++) {
                    if (textSlides[i].classList.contains("show")) {
                        textSlides[
                            i
                        ].style = `transform: translateX(${-slideWidth}px); transition: 0.5s`;
                        imgSlides[i].classList.remove("show");
                        setTimeout(() => {
                            textSlides[i].classList.remove("show");
                            textSlides[i].style = "";
                        }, 500);
                    } else {
                        textSlides[
                            i
                        ].style = `transform: translateX(0); transition: 0.5s; opacity: 1`;
                        imgSlides[i].classList.add("show");
                        setTimeout(() => {
                            textSlides[i].classList.add("show");
                            textSlides[i].style = "";
                        }, 500);
                    }
                }
            } else {
                for (let i = 0; i < textSlides.length; i++) {
                    if (textSlides[i].classList.contains("show")) {
                        textSlides[
                            i
                        ].style = `transform: translateX(${0}); transition: 0.5s`;
                        setTimeout(() => {
                            textSlides[i].style = "";
                        }, 500);
                    } else {
                        textSlides[
                            i
                        ].style = `transform: translateX(${slideWidth}px); transition: 0.5s`;
                        setTimeout(() => {
                            textSlides[i].style = "";
                        }, 500);
                    }
                }
            }
        }
    }
});
document.addEventListener("mousemove", (e) => {
    if (isDown) {
        console.log(distance1);
        let move = e.clientX - start;
        for (let i = 0; i < textSlides.length; i++) {
            if (textSlides[i].classList.contains("show")) {
                let count = Math.ceil(move / slideWidth);
                if (count % 2 === 0) {
                    distance1 = move - slideWidth * count;
                } else distance1 = move - slideWidth * (count - 1);
                textSlides[i].style = `transform : translateX(${distance1}px)`;
            } else {
                let count = Math.ceil(move / slideWidth);
                if (count % 2 === 0) {
                    distance2 = move - slideWidth * (count - 1);
                } else distance2 = move - slideWidth * count;
                textSlides[
                    i
                ].style = `transform : translateX(${distance2}px); opacity: 1`;
            }
        }
    }
});
