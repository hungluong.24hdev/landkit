const datatables = [
    {
        name: "Tiger Nixon",
        position: "System Architect",
        office: "Edinburgh",
        age: "61",
        startDate: "2011/04/25",
        salary: "$320,800",
    },
    {
        name: "Garrett Winters",
        position: "Accountant",
        office: "Tokyo",
        age: "63",
        startDate: "2011/07/25",
        salary: "$170,750",
    },
    {
        name: "Ashton Cox",
        position: "Junior Technical Author",
        office: "San Francisco",
        age: "66",
        startDate: "2009/01/12",
        salary: "$86,000",
    },
    {
        name: "Cedric Kelly",
        position: "Senior Javascript Developer",
        office: "Edinburgh",
        age: "22",
        startDate: "2012/03/29",
        salary: "$433,060",
    },
    {
        name: "Airi Satou",
        position: "Accountant",
        office: "Tokyo",
        age: "33",
        startDate: "2008/11/28",
        salary: "$162,700",
    },
    {
        name: "Brielle Williamson",
        position: "Integration Specialist",
        office: "New York",
        age: "61",
        startDate: "2012/12/02",
        salary: "$372,000",
    },
    {
        name: "Herrod Chandler",
        position: "Sales Assistant",
        office: "San Francisco",
        age: "59",
        startDate: "2012/08/06",
        salary: "$137,500",
    },
    {
        name: "Rhona Davidson",
        position: "Integration Specialist",
        office: "Tokyo",
        age: "55",
        startDate: "2010/10/14",
        salary: "$327,900",
    },
    {
        name: "Colleen Hurst",
        position: "Javascript Developer",
        office: "San Francisco",
        age: "39",
        startDate: "2009/09/15",
        salary: "$205,500",
    },
    {
        name: "Sonya Frost",
        position: "Software Engineer",
        office: "Edinburgh",
        age: "23",
        startDate: "2008/12/13",
        salary: "$103,600",
    },
    {
        name: "Jena Gaines",
        position: "Office Manager",
        office: "London",
        age: "30",
        startDate: "2008/12/19",
        salary: "$90,560",
    },
    {
        name: "Quinn Flynn",
        position: "Support Lead",
        office: "Edinburgh",
        age: "22",
        startDate: "2013/03/03",
        salary: "$342,000",
    },
    {
        name: "Charde Marshall",
        position: "Regional Director",
        office: "San Francisco",
        age: "36",
        startDate: "2008/10/16",
        salary: "$470,600",
    },
    {
        name: "Haley Kennedy",
        position: "Senior Marketing Designer",
        office: "London",
        age: "43",
        startDate: "2012/12/18",
        salary: "$313,500",
    },
    {
        name: "Tatyana Fitzpatrick",
        position: "Regional Director",
        office: "London",
        age: "19",
        startDate: "2010/03/17",
        salary: "$385,750",
    },
    {
        name: "Michael Silva",
        position: "Marketing Designer",
        office: "London",
        age: "66",
        startDate: "2012/11/27",
        salary: "$198,500",
    },
    {
        name: "Paul Byrd",
        position: "Chief Financial Officer (CFO)",
        office: "New York",
        age: "64",
        startDate: "2010/06/09",
        salary: "$725,000",
    },
    {
        name: "Gloria Little",
        position: "Systems Administrator",
        office: "New York",
        age: "59",
        startDate: "2009/04/10",
        salary: "$237,500",
    },
    {
        name: "Bradley Greer",
        position: "Software Engineer",
        office: "London",
        age: "41",
        startDate: "2012/10/13",
        salary: "$132,000",
    },
    {
        name: "Dai Rios",
        position: "Personnel Lead",
        office: "Edinburgh",
        age: "35",
        startDate: "2012/09/26",
        salary: "$217,500",
    },
    {
        name: "Jenette Caldwell",
        position: "Development Lead",
        office: "New York",
        age: "30",
        startDate: "2011/09/03",
        salary: "$345,000",
    },
    {
        name: "Yuri Berry",
        position: "Chief Marketing Officer (CMO)",
        office: "New York",
        age: "40",
        startDate: "2009/06/25",
        salary: "$675,000",
    },
    {
        name: "Caesar Vance",
        position: "Pre-Sales Support",
        office: "New York",
        age: "21",
        startDate: "2011/12/12",
        salary: "$106,450",
    },
    {
        name: "Doris Wilder",
        position: "Sales Assistant",
        office: "Sydney",
        age: "23",
        startDate: "2010/09/20",
        salary: "$85,600",
    },
    {
        name: "Angelica Ramos",
        position: "Chief Executive Officer (CEO)",
        office: "London",
        age: "47",
        startDate: "2009/10/09",
        salary: "$1,200,000",
    },
    {
        name: "Gavin Joyce",
        position: "Developer",
        office: "Edinburgh",
        age: "42",
        startDate: "2010/12/22",
        salary: "$92,575",
    },
    {
        name: "Jennifer Chang",
        position: "Regional Director",
        office: "Singapore",
        age: "28",
        startDate: "2010/11/14",
        salary: "$357,650",
    },
    {
        name: "Brenden Wagner",
        position: "Software Engineer",
        office: "San Francisco",
        age: "28",
        startDate: "2011/06/07",
        salary: "$206,850",
    },
    {
        name: "Fiona Green",
        position: "Chief Operating Officer (COO)",
        office: "San Francisco",
        age: "48",
        startDate: "2010/03/11",
        salary: "$850,000",
    },
    {
        name: "Shou Itou",
        position: "Regional Marketing",
        office: "Tokyo",
        age: "20",
        startDate: "2011/08/14",
        salary: "$163,000",
    },
    {
        name: "Michelle House",
        position: "Integration Specialist",
        office: "Sydney",
        age: "37",
        startDate: "2011/06/02",
        salary: "$95,400",
    },
    {
        name: "Suki Burks",
        position: "Developer",
        office: "London",
        age: "53",
        startDate: "2009/10/22",
        salary: "$114,500",
    },
    {
        name: "Prescott Bartlett",
        position: "Technical Author",
        office: "London",
        age: "27",
        startDate: "2011/05/07",
        salary: "$145,000",
    },
    {
        name: "Gavin Cortez",
        position: "Team Leader",
        office: "San Francisco",
        age: "22",
        startDate: "2008/10/26",
        salary: "$235,500",
    },
    {
        name: "Martena Mccray",
        position: "Post-Sales support",
        office: "Edinburgh",
        age: "46",
        startDate: "2011/03/09",
        salary: "$324,050",
    },
    {
        name: "Unity Butler",
        position: "Marketing Designer",
        office: "San Francisco",
        age: "47",
        startDate: "2009/12/09",
        salary: "$85,675",
    },
    {
        name: "Howard Hatfield",
        position: "Office Manager",
        office: "San Francisco",
        age: "51",
        startDate: "2008/12/16",
        salary: "$164,500",
    },
    {
        name: "Hope Fuentes",
        position: "Secretary",
        office: "San Francisco",
        age: "41",
        startDate: "2010/02/12",
        salary: "$109,850",
    },
    {
        name: "Vivian Harrell",
        position: "Financial Controller",
        office: "San Francisco",
        age: "62",
        startDate: "2009/02/14",
        salary: "$452,500",
    },
    {
        name: "Timothy Mooney",
        position: "Office Manager",
        office: "London",
        age: "37",
        startDate: "2008/12/11",
        salary: "$136,200",
    },
    {
        name: "Jackson Bradshaw",
        position: "Director",
        office: "New York",
        age: "65",
        startDate: "2008/09/26",
        salary: "$645,750",
    },
    {
        name: "Olivia Liang",
        position: "Support Engineer",
        office: "Singapore",
        age: "64",
        startDate: "2011/02/03",
        salary: "$234,500",
    },
    {
        name: "Bruno Nash",
        position: "Software Engineer",
        office: "London",
        age: "38",
        startDate: "2011/05/03",
        salary: "$163,500",
    },
    {
        name: "Sakura Yamamoto",
        position: "Support Engineer",
        office: "Tokyo",
        age: "37",
        startDate: "2009/08/19",
        salary: "$139,575",
    },
    {
        name: "Thor Walton",
        position: "Developer",
        office: "New York",
        age: "61",
        startDate: "2013/08/11",
        salary: "$98,540",
    },
    {
        name: "Finn Camacho",
        position: "Support Engineer",
        office: "San Francisco",
        age: "47",
        startDate: "2009/07/07",
        salary: "$87,500",
    },
    {
        name: "Serge Baldwin",
        position: "Data Coordinator",
        office: "Singapore",
        age: "64",
        startDate: "2012/04/09",
        salary: "$138,575",
    },
    {
        name: "Zenaida Frank",
        position: "Software Engineer",
        office: "New York",
        age: "63",
        startDate: "2010/01/04",
        salary: "$125,250",
    },
    {
        name: "Zorita Serrano",
        position: "Software Engineer",
        office: "San Francisco",
        age: "56",
        startDate: "2012/06/01",
        salary: "$115,000",
    },
    {
        name: "Jennifer Acosta",
        position: "Junior Javascript Developer",
        office: "Edinburgh",
        age: "43",
        startDate: "2013/02/01",
        salary: "$75,650",
    },
    {
        name: "Cara Stevens",
        position: "Sales Assistant",
        office: "New York",
        age: "46",
        startDate: "2011/12/06",
        salary: "$145,600",
    },
    {
        name: "Hermione Butler",
        position: "Regional Director",
        office: "London",
        age: "47",
        startDate: "2011/03/21",
        salary: "$356,250",
    },
    {
        name: "Lael Greer",
        position: "Systems Administrator",
        office: "London",
        age: "21",
        startDate: "2009/02/27",
        salary: "$103,500",
    },
    {
        name: "Jonas Alexander",
        position: "Developer",
        office: "San Francisco",
        age: "30",
        startDate: "2010/07/14",
        salary: "$86,500",
    },
    {
        name: "Shad Decker",
        position: "Regional Director",
        office: "Edinburgh",
        age: "51",
        startDate: "2008/11/13",
        salary: "$183,000",
    },
    {
        name: "Michael Bruce",
        position: "Javascript Developer",
        office: "Singapore",
        age: "29",
        startDate: "2011/06/27",
        salary: "$183,000",
    },
    {
        name: "Donna Snider",
        position: "Customer Support",
        office: "New York",
        age: "27",
        startDate: "2011/01/25",
        salary: "$112,000",
    },
];
const tbody = document.querySelector("tbody");
const rows = document.querySelectorAll("tbody tr");
const theads = document.querySelectorAll(".sorting");
const btn_next = document.querySelector(".btn-next");
const btn_pre = document.querySelector(".btn-pre");
const paginationNumberBox = document.querySelector(".page-number-box");
const selectBox = document.querySelector(".select-box");
const searchBox = document.querySelector(".search-field");
const status_rows = document.querySelector(".status");

let tempData = [...datatables];
const T_HEAD = Object.keys(tempData[0]);

let limit = selectBox.value;
let start;
let end;
let tbody_data = "";
let pageNumber = 1;
let totalPage = Math.ceil(tempData.length / limit);
let sortBy = 0;

const renderData = (data) => {
    l = limit;
    p = pageNumber;
    start = (p - 1) * l;
    end = p * l;
    tbody_data = data.reduce((pre, curr, index) => {
        if (index >= start && index < end) {
            return (
                pre +
                `<tr>
                      <td>${curr.name}</td>
                      <td>${curr.position}</td>
                      <td>${curr.office}</td>
                      <td>${curr.age}</td>
                      <td>${curr.startDate}</td>
                      <td>${curr.salary}</td>
                  </tr>`
            );
        } else return pre;
    }, "");

    let showEnd;
    if (end > data.length) {
        showEnd = data.length;
    } else showEnd = end;
    tbody.innerHTML = tbody_data;
    status_rows.innerHTML = `Showing ${start + 1} to ${showEnd} of ${
        data.length
    } entries`;
};
const changePageActive = (listPage, index) => {
    for (let j = 0; j < listPage.length; j++) {
        listPage[j].classList.remove("active-page");
        listPage[j].disabled = false;
    }
    listPage[index].classList.add("active-page");
    listPage[index].disabled = true;
};

const renderListPage = () => {
    let htmlNumberPage = "";
    for (let i = 1; i <= totalPage; i++) {
        htmlNumberPage += `<button class="num-pagination">${i}</button>`;
    }
    paginationNumberBox.innerHTML = htmlNumberPage;
    if (pageNumber === 1) {
        btn_pre.disabled = true;
        const page1 = document.querySelectorAll(".num-pagination")[0];
        page1.classList.add("active-page");
        page1.disabled = true;
    } else btn_pre.disabled = false;
    if (pageNumber >= totalPage) btn_next.disabled = true;
    else btn_next.disabled = false;
};

btn_next.addEventListener("click", () => {
    ++pageNumber;
    renderData(tempData);

    const pages = document.querySelectorAll(".num-pagination");
    pages[pageNumber - 1].classList.add("active-page");
    pages[pageNumber - 2].classList.remove("active-page");
    pages[pageNumber - 1].disabled = true;
    pages[pageNumber - 2].disabled = false;

    if (pageNumber === totalPage) btn_next.disabled = true;
    if (pageNumber > 1) btn_pre.disabled = false;
});

btn_pre.addEventListener("click", () => {
    --pageNumber;
    renderData(tempData);

    const pages = document.querySelectorAll(".num-pagination");
    pages[pageNumber - 1].classList.add("active-page");
    pages[pageNumber].classList.remove("active-page");
    pages[pageNumber - 1].disabled = true;
    pages[pageNumber].disabled = false;

    if (pageNumber === 1) btn_pre.disabled = true;
    if (pageNumber < totalPage) btn_next.disabled = false;
});

const handleChagePage = () => {
    const btn_numbers = document.querySelectorAll(".num-pagination");
    for (let i = 0; i < btn_numbers.length; i++) {
        btn_numbers[i].addEventListener("click", () => {
            if (i + 1 !== pageNumber) {
                pageNumber = i + 1;
                renderData(tempData);
                changePageActive(btn_numbers, i);
                if (pageNumber === totalPage) btn_next.disabled = true;
                else btn_next.disabled = false;
                if (pageNumber === 1) btn_pre.disabled = true;
                else btn_pre.disabled = false;
            }
        });
    }
};

const sortData = (sortBy = 0, dir = "asc") => {
    const asc = (a, b) => {
        x = a[T_HEAD[sortBy]];
        y = b[T_HEAD[sortBy]];
        if (sortBy === 5) {
            x = parseInt(x.replaceAll("$", "").replaceAll(",", ""));
            y = parseInt(y.replaceAll("$", "").replaceAll(",", ""));
        }
        if (sortBy === 3) {
            x = parseInt(x);
            y = parseInt(y);
        }
        if (x > y) return 1;
        if (x < y) return -1;
        return 0;
    };
    const des = (a, b) => {
        x = a[T_HEAD[sortBy]];
        y = b[T_HEAD[sortBy]];
        if (sortBy === 5) {
            x = parseInt(x.replaceAll("$", "").replaceAll(",", ""));
            y = parseInt(y.replaceAll("$", "").replaceAll(",", ""));
        }
        if (sortBy === 3) {
            x = parseInt(x);
            y = parseInt(y);
        }
        if (x > y) return -1;
        if (x < y) return 1;
        return 0;
    };
    if (dir === "asc") {
        tempData = tempData.sort(asc);
    }
    if (dir === "des") {
        tempData = tempData.sort(des);
    }
};

// initiation
sortData(sortBy, "asc");
theads[0].classList.add("sort-asc");
renderListPage();
renderData(tempData);
handleChagePage();

// //event sort when click thead
for (let i = 0; i < theads.length; i++) {
    theads[i].addEventListener("click", () => {
        sortBy = i;
        if (theads[i].classList.contains("sort-asc")) {
            sortData(sortBy, "des");
            theads[i].classList.remove("sort-asc");
            theads[i].classList.add("sort-des");
            renderData(tempData);
        } else {
            theads[i].classList.remove("sort-des");
            sortData(sortBy, "asc");
            theads[i].classList.add("sort-asc");
            renderData(tempData);
        }
        for (let j = 0; j < theads.length; j++) {
            if (j !== i) {
                theads[j].classList.remove("sort-des");
                theads[j].classList.remove("sort-asc");
            }
        }
    });
}

//change rows per page
const changeLitmitRows = (value) => {
    pageNumber = 1;
    limit = value;
    totalPage = Math.ceil(tempData.length / limit);
    if (theads[sortBy].classList.contains("sort-asc")) sortData(sortBy, "asc");
    else sortData(sortBy, "des");
    renderListPage();
    handleChagePage();
    renderData(tempData);
};
selectBox.addEventListener("change", (e) => changeLitmitRows(e.target.value));

// search
const handleSearch = (value) => {
    tempData = datatables.filter((item) => {
        let arrayDataChildren = Object.values(item);
        return arrayDataChildren.some((e) => e.toLowerCase().includes(value));
    });
    pageNumber = 1;
    totalPage = Math.ceil(tempData.length / limit);
    sortData(sortBy, "asc");
    renderListPage();
    renderData(tempData);
    handleChagePage();
};
searchBox.addEventListener("keyup", (e) =>
    handleSearch(e.target.value.toLowerCase())
);
