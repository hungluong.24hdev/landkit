const welcome_cols= document.querySelectorAll(".welcome-body-col");
const feature_cols= document.querySelectorAll(".features-col");
const about_cols = document.querySelectorAll(".about-col")
const pricing_cols = document.querySelectorAll(".card-pricing-col")

window.addEventListener("load",()=>{
    for(let i=0; i<welcome_cols.length; i++){
        if(welcome_cols[i].getBoundingClientRect().top <= (window.innerHeight || document.documentElement.clientHeight)){
            welcome_cols[i].classList.add("appear-element-1")
        };
    }
    for(let i=0; i<feature_cols.length; i++){
        if(feature_cols[i].getBoundingClientRect().top <= (window.innerHeight || document.documentElement.clientHeight)){
            feature_cols[i].classList.add("appear-element-1")
        };
    }
    for(let i=0; i<about_cols.length; i++){
        if(about_cols[i].getBoundingClientRect().top <= (window.innerHeight || document.documentElement.clientHeight)){
            about_cols[i].classList.add("appear-element-1")
        };
    }
    for(let i=0; i<pricing_cols.length; i++){
        if(pricing_cols[i].getBoundingClientRect().top <= (window.innerHeight || document.documentElement.clientHeight)){
            pricing_cols[i].classList.add("appear-element-1")
        };
    }
})

window.addEventListener("scroll",()=>{
    for(let i=0; i<feature_cols.length; i++){
        if(feature_cols[i].getBoundingClientRect().top <= (window.innerHeight || document.documentElement.clientHeight)){
            feature_cols[i].classList.add("appear-element-1")
        };
    }
    for(let i=0; i<welcome_cols.length; i++){
        if(welcome_cols[i].getBoundingClientRect().top <= (window.innerHeight || document.documentElement.clientHeight)){
            welcome_cols[i].classList.add("appear-element-1")
        };
    }
    for(let i=0; i<about_cols.length; i++){
        if(about_cols[i].getBoundingClientRect().top <= (window.innerHeight || document.documentElement.clientHeight)){
            about_cols[i].classList.add("appear-element-1")
        };
    }
    for(let i=0; i<pricing_cols.length; i++){
        if(pricing_cols[i].getBoundingClientRect().top <= (window.innerHeight || document.documentElement.clientHeight)){
            pricing_cols[i].classList.add("appear-element-1")
        };
    }
})