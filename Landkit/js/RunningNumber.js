const rowStats = document.querySelector(".row-stats");
const runningNumber = document.querySelector(".running-number");
const btn_switch = document.querySelector(".btn-switch");
const number_price_card = document.querySelector(".number-price-card");
const number_support_1 = document.querySelector(".text-support-1");
const number_support_2 = document.querySelector(".text-support-2");
const number_sales = document.querySelector(".text-sales");

window.addEventListener("scroll", () => {
    if (
        rowStats.getBoundingClientRect().top <=
            (window.innerHeight || document.documentElement.clientHeight) &&
        !runningNumber.classList.contains("finished-number")
    ) {
        runningNumber.classList.add("finished-number");
        let i = 0;
        let run = setInterval(() => {
            if (i <= 7) {
                number_support_2.innerHTML = `${i}`;
            }
            if (i <= 24) {
                number_support_1.innerHTML = `${i}/`;
            }
            if (i < 100) {
                runningNumber.innerHTML = `${++i}%`;
                number_sales.innerHTML = `${i}k+`;
            } else {
                clearInterval(run);
            }
        }, 10);
    }
});

btn_switch.addEventListener("click", () => {
    let counter = number_price_card.innerText;
    counter = parseInt(counter);
    if (btn_switch.classList.contains("switch-state")) {
        btn_switch.classList.remove("switch-state");
        let run = setInterval(() => {
            number_price_card.innerHTML = `${counter--}`;
            if (counter < 29) {
                clearInterval(run);
            }
        }, 30);
    } else {
        btn_switch.classList.add("switch-state");
        let run = setInterval(() => {
            number_price_card.innerHTML = `${counter++}`;
            if (counter > 49) {
                clearInterval(run);
            }
        }, 30);
    }
});
